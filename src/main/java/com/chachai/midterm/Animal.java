package com.chachai.midterm;

public class Animal {
    private String name;
    private String breed;
    private char symbol;
    private int x;
    private int y;
    public static final int MIN_X = 1;
    public static final int MIN_Y = 1;
    public static final int MAX_X = 10;
    public static final int MAX_Y = 10;

    public Animal(String name, String breed, char symbol, int x, int y) {
        this.name = name;
        this.breed = breed;
        this.symbol = symbol;
        this.x = x;
        this.y = y;
    }

    public boolean up() {
        if (y == MIN_Y) {
            return false;
        }
        y = y - 1;
        return true;
    }

    public boolean up(int step) {
        for (int i = 0; i < step; i++) {
            if (!up()) {
                return false;
            }
        }
        return true;
    }

    public boolean down() {
        if (y == MAX_Y) {
            return false;
        }
        y = y + 1;
        return true;
    }

    public boolean down(int step) {
        for (int i = 0; i < step; i++) {
            if (!down()) {
                return false;
            }
        }
        return true;
    }

    public boolean left() {
        if (x == MIN_X) {
            return false;
        }
        x = x - 1;
        return true;
    }

    public boolean left(int step) {
        for (int i = 0; i < step; i++) {
            if (!left()) {
                return false;
            }
        }
        return true;
    }

    public boolean right() {
        if (x == MAX_X) {
            return false;
        }
        x = x + 1;
        return true;
    }

    public boolean right(int step) {
        for (int i = 0; i < step; i++) {
            if (!right()) {
                return false;
            }
        }
        return true;
    }

    public void print() {
        System.out.println("Animal: " + name + " Breed: " + breed + " X: " + x + " Y: " + y);
    }

    public String getName() {
        return name;
    }

    public String getBreed() {
        return breed;
    }

    public char getSymbol() {
        return symbol;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
