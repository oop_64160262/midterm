package com.chachai.midterm;

public class ShapeLand {
    private double width;
    private double height;
    private float radius;
    private double a;
    private double b;
    private double c;
    
    public ShapeLand(double width,double height){
        this.width = width;
        this.height = height;
    }
    public ShapeLand(float radius){
        this.radius = radius;
    }
    public ShapeLand(double a, double b ,double c){
        this.a = a;
        this.b = b;
        this.c = c;
    }
   
    //Retangle Getter & Setter method
    public double getWidth(){
        return width;
    }
    public double getHeight(){
        return height;
    }
    public void setWidth(double width) {
        this.width = width;
    }
    public void setHeight(double height) {
        this.height = height;
    }
   
    //Rectangle calculate & print method
    public double calReactangleAreaCost(){
        return (width*height)*1000;
    }
    public void printReactangleArea(){
        System.out.println("Reactangle Area Cost: " + calReactangleAreaCost());
    }
   
    //Circle Getter & Setter method
    public double getRadius(){
        return radius;
    }
    public void setRadius(float radius) {
        this.radius = radius;
    }

    //Circle calculate & print method
    public double calCircleAreaCost(){
        return (Math.PI*Math.pow(radius, 2))*1000;
    }
    public void printCircleAreaCost(){
        System.out.println("Circle Area Cost: " + calCircleAreaCost());
    }

    //Triangle Getter & Setter method
    public double getA(){
        return a;
    }
    public double getB(){
        return b;
    }
    public double getC(){
        return c;
    }
    public void setA(double a) {
        this.a = a;
    }
    public void setB(double b) {
        this.b = b;
    }
    public void setC(double c) {
        this.c = c;
    }
    //Triangle calculate & print method
    public double calTriangleAreaCost(){
        double s = (a+b+c)/2;
        return (Math.sqrt(s*(s-a)*(s-b)*(s-c)))*1000;
    }
}