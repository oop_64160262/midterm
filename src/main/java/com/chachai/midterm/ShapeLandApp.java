package com.chachai.midterm;

import java.util.Scanner;

public class ShapeLandApp 
{
    public static void main( String[] args ){
      ShapeLand rect1 = new ShapeLand(10, 10);
      ShapeLand rect2 = new ShapeLand(20, 20);
      ShapeLand circle1 = new ShapeLand(2);
      ShapeLand circle2 = new ShapeLand(4);
      ShapeLand Triangle1 = new ShapeLand(5, 5, 6);
      ShapeLand Triangle2 = new ShapeLand(10, 5, 6);

      System.out.println("Reactangle1 Area Cost: "+ rect1.calReactangleAreaCost() + " " +"BAHT");
      System.out.println("Reactangle2 Area Cost: "+ rect2.calReactangleAreaCost() + " " + "BAHT");
      System.out.println("Circle1 Area Cost: "+ circle1.calCircleAreaCost() + " " + "BAHT");
      System.out.println("Circle2 Area Cost: "+ circle2.calCircleAreaCost() + " "  + "BAHT");
      System.out.println("Triangle1 Area Cost: "+ Triangle1.calTriangleAreaCost() + " " + "BAHT");
      System.out.println("Triangle2 Area Cost: "+ Triangle2.calTriangleAreaCost() + " " + "BAHT");
    }
}
