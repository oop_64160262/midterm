package com.chachai.midterm;

public class AnimalApp {
    public static void main(String[] args) {
        Animal dang = new Animal("Dang", "Thai", 'd', 5, 5);
        Animal lucky = new Animal("Lucky", "Chihuahua", 'l', 2, 9);
        Animal pate = new Animal("Pate", "Thai", 'p', 1, 1);
        dang.print();
        lucky.print();
        pate.print();

        for (int y = Animal.MIN_Y; y <= Animal.MAX_Y; y++) {
            for (int x = Animal.MIN_X; x <= Animal.MAX_X; x++) {
                if (dang.getX() == x && dang.getY() == y) {
                    System.out.print(dang.getSymbol());
                } else if (lucky.getX() == x && lucky.getY() == y) {
                    System.out.print(lucky.getSymbol());
                } else if (pate.getX() == x && pate.getY() == y) {
                    System.out.print(pate.getSymbol());
                } else {
                    System.out.print("-");
                }
            }
            System.out.println();
        }

    }

}
